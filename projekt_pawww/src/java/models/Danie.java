/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import api.DBconn;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author krejz
 */
public class Danie {
    private int id = 0;
    private String name = "";
    private String description = "";
    private String pora_dnia = "";
    private ArrayList<Skladniki> skladniki_list = null;
    private User creator_id = new User();
    private int ile_skladnikow = 0;    
    private int kalorycznosc = 0;

    public void setIle_skladnikow(int ile_skladnikow) {
        this.ile_skladnikow = ile_skladnikow;
    }

    public int getIle_skladnikow() {
        return ile_skladnikow;
    }
   
    public ArrayList<Skladniki> getSkladniki_list() {
        return skladniki_list;
    }

    public void setKalorycznosc(int kalorycznosc) {
        this.kalorycznosc = kalorycznosc;
    }

    public int getKalorycznosc() {
        return kalorycznosc;
    }

    
    
    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreator(User creator_id) {
        this.creator_id = creator_id;
    }

    public String getPora_dnia() {
        return pora_dnia;
    }

    public void setPora_dnia(String pora_dnia) {
        this.pora_dnia = pora_dnia;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public User getCreator_id() {
        return creator_id;
    }

    public String getDescription() {
        return description;
    }
   
    public Danie findDanie(int id){
            String query = "Select * from danie";
                query += " Where id = '"+id+"' LIMIT 1;";

            
            Connection con = DBconn.get_connection();
            try{
                Statement statement = con.createStatement();
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()){
                    this.id = id;
                    this.description = rs.getString("description");
                    this.name = rs.getString("name");
                    this.pora_dnia = rs.getString("pora_dnia");
                    this.kalorycznosc = rs.getInt("kalorycznosc");
                    //znajdź takiego gościa
                    User temp_usr = new User();
                        temp_usr.findUser(rs.getInt("creator_id"), "","");
                    this.creator_id = temp_usr;
                    
                    this.getSkladniki();
                }
                
            }catch (SQLException e){
                System.out.print("Blad " + e);
            }
            return this;
    }
            
            
        public void getSkladniki(){
            skladniki_list = new ArrayList<Skladniki>();
            
            String query = "Select * from danie_skladniki";
                query += " Where danie_id = '"+id+"';";

            
            Connection con = DBconn.get_connection();
            try{
                Statement statement = con.createStatement();
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()){
                    Skladniki tmp = new Skladniki();
                      tmp.setObj_danie(this);
                      tmp.setIlosc(rs.getInt("ilosc"));
                      tmp.setId(rs.getInt("id"));
                      Skladnik tmpskl = new Skladnik();
                        tmpskl.findSkladnik(rs.getInt("skladnik_id"));
                      tmp.setObj_skladnik(tmpskl);
                      
                    skladniki_list.add(tmp);
                }
                
            }catch (SQLException e){
                System.out.print("Blad " + e);
            }
            
        }    
           
        public void parseSkladniki( String dane ){
            
            skladniki_list = new ArrayList<Skladniki>();
            
            if(this.id > 0){
            Connection con = DBconn.get_connection();
            
             String query = "DELETE FROM danie_skladniki";
                    query += " WHERE danie_id = '"+this.id+"';";
                    try{
                    Statement statement = con.createStatement();
                    statement.executeUpdate(query);
                    }catch(SQLException e){
                    System.out.println("Error: " + e);
                    }
            }
            
            int kalorycznosc = 0;
            
            String[] split = dane.split(";");
                for (int i = 0; i < split.length; i++) {
                    //format: id-ilosc
                      String[] skladniki = split[i].split("-");  
                      if(skladniki.length < 2) continue;
                      int id = Integer.parseInt(  ((skladniki[0]!=null)?skladniki[0]:"")  );
                      int ilosc = Integer.parseInt(  ((skladniki[1]!=null)?skladniki[1]:"")  );
                      
                      if(id > 0 && ilosc > 0){
                      Skladniki tmp = new Skladniki();
                      tmp.setObj_danie(this);
                      tmp.setIlosc(ilosc);
                      
                      Skladnik tmpskl = new Skladnik();
                        tmpskl.findSkladnik(id);
                      
                      if(tmpskl.getId() > 0){
                          kalorycznosc += ilosc*tmpskl.getKalorycznosc();
                        tmp.setObj_skladnik(tmpskl);
                        tmp.save();
                        skladniki_list.add(tmp);
                      }
                      
                      }
                }
                
            this.kalorycznosc = kalorycznosc;
        }
            
        
        public void save(){
            Connection con = DBconn.get_connection();
                  
            if(this.id > 0){
                //update
                String query = "UPDATE danie SET name ='"+this.name+"', description='"+this.description+"',";
                        query += "pora_dnia ='"+this.pora_dnia+"', creator_id='"+this.creator_id.getId()+"', kalorycznosc='"+this.kalorycznosc+"' ";
                    query += "WHERE id = '"+this.id+"' LIMIT 1;";
                    try{
                    Statement statement = con.createStatement();
                    statement.executeUpdate(query);
                    }catch(SQLException e){
                    System.out.println("Error: " + e);
                    }
            }else{
                //insert
                String query = "INSERT INTO danie (name, description, pora_dnia, creator_id, kalorycznosc) ";
                    query += "VALUES('"+this.name+"', '"+this.description+"', '"+this.pora_dnia+"', '"+this.creator_id.getId()+"', '"+this.kalorycznosc+"')";
                    try{
                    Statement statement = con.createStatement();
                    statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
                    ResultSet rs = statement.getGeneratedKeys();
                        rs.next(); 
                    int last_id = rs.getInt(1);
                    
                    this.id = last_id;
                        System.err.println("DANIE - nowe id: "+last_id);                  
                    }catch(SQLException e){
                    System.out.println("Error: " + e);
                    }
            }
        }
        
        public void remove(){
            Connection con = DBconn.get_connection();
            
             String query = "DELETE FROM dania";
                    query += " WHERE id = '"+this.id+"' LIMIT 1;";
                    try{
                    Statement statement = con.createStatement();
                    statement.executeUpdate(query);
                    }catch(SQLException e){
                    System.out.println("Error: " + e);
                    }
        }
}
