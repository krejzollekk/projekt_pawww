/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class MyFilter implements Filter {
private static final boolean debug = true;
private FilterConfig filterConfig = null;
private HashMap links = null;
private PrintWriter zapis = null;


public MyFilter() {
}

private void doBeforeProcessing(ServletRequest request, ServletResponse response)
throws IOException, ServletException {
if (debug) {
log("NewFilter1:DoBeforeProcessing");

//logujemy dane wejściowe
        System.out.println("user ip: "+request.getRemoteAddr()+" / "); 

        Enumeration<String> parameterNames = request.getParameterNames();

		while (parameterNames.hasMoreElements()) {

			String paramName = parameterNames.nextElement();
			if(this.zapis != null)zapis.print("> '"+paramName+"'");
			
                            System.out.print("> '"+paramName+"'");
                        
			String[] paramValues = request.getParameterValues(paramName);
			for (int i = 0; i < paramValues.length; i++) {
				String paramValue = paramValues[i];
				if(this.zapis != null)
                                    zapis.println("= '"+paramValue+"'");
                                System.out.println("= '"+paramValue+"'");
			}

		}
    
    
}
}

private void doAfterProcessing(ServletRequest request, ServletResponse response)
throws IOException, ServletException {
if (debug) {
log("NewFilter1:DoAfterProcessing");
}}

public void doFilter(ServletRequest request, ServletResponse response,
FilterChain chain)
throws IOException, ServletException {

if (debug) {
log("NewFilter1:doFilter()");
}

doBeforeProcessing(request, response);

Throwable problem = null;
try {
String link = ((HttpServletRequest) request).getRequestURL().toString();
int count = 0;
if (links.containsKey(link)) {
count = Integer.parseInt(links.get(link).toString());
}
links.put(link, Integer.valueOf(++count));
Iterator i = links.keySet().iterator();
while (i.hasNext()) {
link = i.next().toString();
count = Integer.parseInt(links.get(link).toString());
String str = link + " requested " + count + " times";
this.getFilterConfig().getServletContext().log(str);
}
chain.doFilter(request, response);
} catch (Throwable t) {
problem = t;
t.printStackTrace();
}

doAfterProcessing(request, response);
if (problem != null) {
if (problem instanceof ServletException) {
throw (ServletException) problem;
}
if (problem instanceof IOException) {
throw (IOException) problem;
}
sendProcessingError(problem, response);
}
}

public FilterConfig getFilterConfig() {
return (this.filterConfig);
}

public void setFilterConfig(FilterConfig filterConfig) {
this.filterConfig = filterConfig;
}

public void destroy() {
}

public void init(FilterConfig filterConfig) {
this.filterConfig = filterConfig;
this.links = new HashMap();
    System.out.println("api.MyFilter.init() plik: "+filterConfig.getInitParameter("save-to-file"));

    try {
        this.zapis = new PrintWriter(filterConfig.getInitParameter("save-to-file"));
    } catch (FileNotFoundException ex) {
        Logger.getLogger(MyFilter.class.getName()).log(Level.SEVERE, null, ex);
    }

if (filterConfig != null) {
if (debug) {
log("NewFilter1:Initializing filter");
}
}
}

@Override
public String toString() {
if (filterConfig == null) {
return ("NewFilter1()");
}
StringBuffer sb = new StringBuffer("NewFilter1(");
sb.append(filterConfig);
sb.append(")");
return (sb.toString());
}

private void sendProcessingError(Throwable t, ServletResponse response) {
String stackTrace = getStackTrace(t);

if (stackTrace != null && !stackTrace.equals("")) {
try {
response.setContentType("text/html");
PrintStream ps = new PrintStream(response.getOutputStream());
PrintWriter pw = new PrintWriter(ps);
pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

// PENDING! Localize this for next official release
pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
pw.print(stackTrace);
pw.print("</pre></body>\n</html>"); //NOI18N
pw.close();
ps.close();
response.getOutputStream().close();
} catch (Exception ex) {
}
} else {
try {
PrintStream ps = new PrintStream(response.getOutputStream());
t.printStackTrace(ps);
ps.close();
response.getOutputStream().close();
} catch (Exception ex) {
}
}
}

public static String getStackTrace(Throwable t) {
String stackTrace = null;
try {
StringWriter sw = new StringWriter();
PrintWriter pw = new PrintWriter(sw);
t.printStackTrace(pw);
pw.close();
sw.close();
stackTrace = sw.getBuffer().toString();
} catch (Exception ex) {
}
return stackTrace;
}

public void log(String msg) {
filterConfig.getServletContext().log(msg);
}

}