package api;

import javax.servlet.http.HttpSession;
import models.User;

public class Authenticator {
 
    private HttpSession session = null;
    
    
    public Authenticator(HttpSession sess){
        this.session = sess;
    }
    
	public User authenticate(String email, String password) {
            User temp = new User();
                temp.findUser(0, email, password);
            
		if ( temp.getId() > 0){
                       session.setAttribute("email", email); 
                       session.setAttribute("password", password);
			return temp;
		} else {
			return null;
		}
	}
        
        public User isLogged(){
                     User temp = new User();
                    temp.findUser(0, (String)session.getAttribute("email"), (String)session.getAttribute("password"));  
                    
                    return (temp.getId() > 0)?temp:null;
        }
                
        public void logout(){
                System.out.println("usuwam sesje");
            if(session != null){
                session.invalidate();
                
            }
        }
        
        public User createUser( String email, String password ){
            User temp = new User(email, password);
                temp.save();
            return temp;
        }
} 