/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import api.Authenticator;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.User;

/**
 *
 * @author Maciej
 */
@WebServlet(name = "KontoController", urlPatterns = {"/KontoController"})
public class KontoController extends HttpServlet {

    public KontoController() {
        super();
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        
        
        
        RequestDispatcher rd = null;
        HttpSession session = request.getSession();
        Authenticator auth = new Authenticator(session);
        User logged = auth.isLogged();
            rd = request.getRequestDispatcher("/konto.jsp");
           
            if(logged == null)
               rd = request.getRequestDispatcher("/login.jsp");
            
            rd.forward(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        RequestDispatcher rd = null;
        HttpSession session = request.getSession();
                Authenticator auth = new Authenticator(session);
                User logged = auth.isLogged();
                
                 if(logged != null){
                     String edit_id = request.getParameter("user_id");
                     String Email = request.getParameter("email");
                     String StareHaslo = request.getParameter("starehaslo");
                     String Haslo = request.getParameter("haslo");
                     int id = 0;
                      id=Integer.parseInt(edit_id);
                     
                     User temp = new User();
                     temp.findUser(id,Email,StareHaslo );
                    temp.setPassword(Haslo);
                    //System.out.print(temp.getPassword());
                     
                     temp.save();
        rd = request.getRequestDispatcher("/success1.jsp");
                 }
                 else{
                      rd = request.getRequestDispatcher("/error.jsp");
                 }
                 
                 
        rd.forward(request, response);  
    }

    

}
