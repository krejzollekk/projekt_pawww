/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import api.Authenticator;
import api.Core;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.Danie;
import models.User;
import static sun.security.jgss.GSSUtil.login;

@WebServlet(name = "MainController", urlPatterns = {"/MainController"})
public class MainController extends HttpServlet {

    public MainController() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = null;
          
        
        String search = request.getParameter("search");
        int page = Core.ConvToInt(request.getParameter("page"));
        String pora_dnia = request.getParameter("pora_dnia");
        int kcal_od = Core.ConvToInt(request.getParameter("kcal_od"));
        int kcal_do = Core.ConvToInt(request.getParameter("kcal_do"));
        
        

  
            request.setAttribute("page", page);
            request.setAttribute("search", search);
            request.setAttribute("pora_dnia", pora_dnia);
            request.setAttribute("kcal_od", kcal_od); 
            request.setAttribute("kcal_do", kcal_do);
            
            //COOKIES?
            String ciastko = "";    String a = ""; String b = search;
            System.out.println(ciastko);    
            Cookie czyta = new Cookie("beznazwy","");
            if(b == null){
                ciastko = ciastko;
            czyta.setValue(ciastko);
           }
            else{
                ciastko = ciastko +b;
                czyta.setValue(ciastko);
            }
            
            czyta.setMaxAge(60*60*24); 
            response.addCookie(czyta);
            
            
            //ZNACZNIK JSTL YOLO
            Core funkcje = new Core();
            ArrayList<String> list = funkcje.getPoraDnia_options();
            request.setAttribute("lista_poradnia", list );
            
            ArrayList<Danie> results = funkcje.ListAllDania(search,pora_dnia,kcal_od,kcal_do,page,10);
            
            request.setAttribute("lista_results", results );
            
         rd = request.getRequestDispatcher("/dania_list.jsp");   

        rd.forward(request, response);
            
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
}
    

}
