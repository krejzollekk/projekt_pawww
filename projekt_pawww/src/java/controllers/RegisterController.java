/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import api.Authenticator;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.User;

/**
 *
 * @author krejz
 */
@WebServlet(name = "RegisterController", urlPatterns = {"/RegisterController"})
public class RegisterController extends HttpServlet {

    public RegisterController() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            RequestDispatcher rd = null;
            rd = request.getRequestDispatcher("/register.jsp");
            rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            RequestDispatcher rd = null;
            
            	String email = request.getParameter("email");
		String password = request.getParameter("password");
                String firstname = request.getParameter("firstname");
                String lastname = request.getParameter("lastname");
                
                
                    HttpSession session = request.getSession();
                    Authenticator auth = new Authenticator(session); // OBSŁUGA SESJI I LOGOWANKA
                    
                    User temp = auth.createUser( email, password );
                       temp.setFirstname(firstname);
                       temp.setLastname(lastname);
                       temp.save();
                       
                       rd = request.getRequestDispatcher("/success.jsp");
            
            
            rd.forward(request, response);
    }


}
