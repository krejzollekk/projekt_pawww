/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Skladnik;

@WebServlet(name = "SkladnikController", urlPatterns = {"/SkladnikController"})
public class SkladnikController extends HttpServlet {

    public SkladnikController() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = null;
            rd = request.getRequestDispatcher("/MainController");
        rd.forward(request, response);
            
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String result = "";
        
        String nazwa = request.getParameter("name");
        String kalorycznosc = request.getParameter("kalorycznosc");
        int kcal = 0;
            if(kalorycznosc != null) kcal=Integer.parseInt(kalorycznosc);
        
            if(nazwa.length() > 2 && kcal > 0){
                Skladnik temp = new Skladnik();
                    temp.setName(nazwa);
                    temp.setKalorycznosc(kcal);
                temp.save();
                
                result = ""+temp.getId(); // zwracam id 
            }else{
                result = "Nazwa za krótka(min. dwa znaki) lub 0 kcal. ";
            }
        
                response.setContentType("text/html;charset=UTF-8");
                try (PrintWriter out = response.getWriter()) {
                    out.println(result);
                }
    }
}
