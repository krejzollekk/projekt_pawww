<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.ArrayList"%>
<%@page import="models.Danie"%>
<%@page import="api.Core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%@ include file="static/header.jsp" %>
    
    <%
        
        String search = "";
        String pora_dnia = "";
        int kcal_od = 0;
        int kcal_do = 0;
        if(request.getAttribute("search") != null) search = (String)request.getAttribute("search");
        if(request.getAttribute("pora_dnia") != null) pora_dnia = (String)request.getAttribute("pora_dnia");
        if(request.getAttribute("kcal_od") != null) kcal_od = (int)request.getAttribute("kcal_od");
        if(request.getAttribute("kcal_do") != null) kcal_do = (int)request.getAttribute("kcal_do");
        

///( (pora_dnia.equals(pozycja))?"selected":"" )
    %>
    <div class="box_main_include" style="width:75%; min-height:300px;">
    <div id="container_title">Wszystkie dania</div>
    <div id="container_content">
        <div>
        <form action="MainController" method="get">
            <b>Wyszukaj:</b> <input name="search" type="text" style="width:300px;" placeholder="Wyszukaj..." value="${search}" />  | 
            pora dnia: <select name="pora_dnia" size="1">
                <option value="">-----------</option>
                    <c:forEach var="element" items="${lista_poradnia}" >
                        <option  value="${element}">${element}</option>
                    </c:forEach>
                </select>
              
                | kaloryczność: <input style="width:60px;" name="kcal_od" type="number" placeholder="od" value="<%=kcal_od%>"/> 
                            <input style="width:60px;" name="kcal_do" type="number" placeholder="do" value="<%=( (kcal_do>0)?kcal_do:"" )%>"/> 
            <input type="submit" value="Wyszukaj" />
        </form>
        </div>
 <table class="dania_list" cellspacing="0">
  <tr>
    <td width="64%">Nazwa</td>
    <td width="12%">Pora dnia</td> 
    <td width="12%">Kaloryczność</td>
    <td width="12%">ilość<br/>składników</td>
  </tr>

  <c:forEach var="element" items="${lista_results}" >
        <tr>
            <td><a href="DanieController?danie_id=${element.getId()}">${element.getName()}</a></td>
          <td>${element.getPora_dnia()}</td> 
          <td>${element.getKalorycznosc()}</td>
          <td>${element.getIle_skladnikow()}</td>
        </tr>
  </c:forEach>

</table>
<% if((int)request.getAttribute("page") > 0){ %>
<a href="MainController?page=<%=(int)request.getAttribute("page")-1%>&search=<%=search%>">Poprzednia strona</a> | 
<% } %>
<a href="MainController?page=<%=(int)request.getAttribute("page")+1%>&search=<%=search%>">Następna strona</a>
        
    </div></div>
    <%@ include file="static/footer.jsp" %>